# Templates

This project contains shared issue templates for all Appsemble projects.

## License

[LGPL-3.0-only](LICENSE.md) © [Appsemble](https://appsemble.com)
