### Description

<!-- Please provide a sentence describing what the user wants to be added in the form of "As a ..., I want ..., so that ..." -->

<!-- Please add screenshots if relevant. 🙂  -->

### Acceptance criteria

<!-- A checklist of acceptance criteria in the form of "Given ..., when ..., then ..." -->

- [ ] Given the demo user ..., when they ..., should ...

<!-- Please add the app label that the user story is related to if relevant. -->

/label ~Story ~"app::Example App" 

/estimate 1d
